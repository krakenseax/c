#include <iostream>

using namespace std;

int main()
{
   const float coffeecost= 25;
   const int fixcost=15;
   const float addcost=2.50;
   int cuporder;
   float costoforder;

   cout<<"Please enter number of cups ordered: \n  ";
   cin >>cuporder;
   costoforder=(((coffeecost*cuporder)+fixcost)+(addcost*cuporder));
   cout << "The cost of order is: RS" << costoforder;

    return 0;
}
