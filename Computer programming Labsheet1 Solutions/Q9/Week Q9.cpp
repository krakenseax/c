#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int x1,x2,y1,y2;
    double d;
    cout << "Enter your first coordinates:\n";
    cin >> x1 >> y1;
    cout << "Enter your second coordinates:\n";
    cin >> x2 >>y2;

    d = sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
    cout << d;

    return 0;
}
