#include <iostream>

using namespace std;

int main()
{
    const float pi = 3.142;
    float h1 ,h2 ,h , r1 ,r2 ,r , v1 ,v2 ,v ,p ;
    cout << "Input height of the pillar : \n";
    cin>> h ;
    cout <<"Input radius of the pillar: \n" ;
    cin >> r ;
    cout << " Input height of the first inner hole : \n";
    cin  >>h1;
    cout << "Input first inner hole radius : \n";
    cin>>r1;

    cout << "Input second inner hole radius : \n" ;
    cin >> r2;

    // Volume of cylinder = pi*r*r*h
    h=(h1+h2);
    v1=(pi*r1*r1*h1);
    v2=(pi*r2*r2*h2);
    v=(pi*r*r*h);
    p=(v-(v1+v2));

    cout<<  "Volume required to construct the pillar is : " << p ;
    return 0;
}
