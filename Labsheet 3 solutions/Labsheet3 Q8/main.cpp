#include <iostream>
using namespace std;

int main ()
{
    char point;

    cout << "Input your score in the 5-point quiz: ";
    cin >> point;

   switch(point) {
      case '5' :
         cout << "A" << endl;
         break;
      case '4' :
         cout << "B" << endl;
         break;
      case '3' :
         cout << "C" << endl;
         break;
      case '2' :
         cout << "D" << endl;
         break;
      case '1' :
        cout << "E" << endl;
        break;
      case '0' :
        cout << "F" << endl;
        break;
      default :
         cout << "Invalid grade" << endl;
   }

   return 0;
}
