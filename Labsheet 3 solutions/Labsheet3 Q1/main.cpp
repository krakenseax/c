#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double radius ,area;
    cout<< "Please input the radius of the circle: ";
    cin>> radius;

    if (radius<1){
        cout<<"Please input a value greater than 0";
    }
    else{
        area=(M_PI * pow(radius,2));
        cout << "Area of circle is :" << area;
    }
    return 0;
}
