#include <iostream>

using namespace std;

int main()
{
    int marks ;

    cout << "Please enter your grade: ";
    cin >> marks;

    if ((marks >= 70)&& (marks <= 100)){
        cout << "Your grade is A";
    }
    else if ((marks >= 60) && (marks <=69)){
        cout <<"Your grade is B";
    }
    else if ((marks >= 50) && (marks <=59)){
        cout <<"Your grade is C";
    }
    else if ((marks >= 40) && (marks <=49)){
        cout <<"Your grade is D";
    }
    else if ((marks < 40)){
        cout <<"Your grade is F";
    }
    else{
        cout << "The marks must be between 0 and 100";
    }

    return 0;
}
