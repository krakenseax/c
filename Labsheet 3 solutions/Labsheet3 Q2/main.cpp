#include <iostream>

using namespace std;

int main()
{
    int year, age ;
    cout << "Input your year of birth:";
    cin >> year;
    age=(2017-year);

    if (age<18){
        cout << "You are a child aged <"<<age<<">!";
    }
    else{
        cout << "You are an adult aged "<<age<<" years old";
    }
    return 0;
}
