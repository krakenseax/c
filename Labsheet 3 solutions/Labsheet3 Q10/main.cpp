#include <iostream>

using namespace std;

int main()
{
   int speed,fine;

   cout << "Input speed in Km/Hr \n";
   cin >> speed;

   if ((speed < 90) && (speed >=0)){
    cout << "The speed limit has not been exceeded.";
   }
   else if ((speed > 90) && (speed<=300)){
    fine=(500+((speed-90)*10));
    cout << "The amount of fine that has to be paid is RS" << fine;
   }
   else{
    cout << "Error! The speed must be between 0 and 300 Km/Hr";
   }
    return 0;
}
