#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double num , square ;

    cout << "Enter a number to check whether it is a perfect sqaure: ";
    cin >> num;

    square = sqrt(num);
    if ((square * square)==num){
        cout << "The " << num <<" is a perfect square and the square root is " << square;
    }
    else{
        cout <<"The " << num << " is not a perfect square";
    }
    return 0;
}
