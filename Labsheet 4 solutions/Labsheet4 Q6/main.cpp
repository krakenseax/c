#include <iostream>

using namespace std;

int main()
{
    int typeOfcars;

    cout << "Please enter a number for the type of car to get descriptions.\nPlease choose between 1, 2, 3, 4, 5.\n";
    cin >> typeOfcars;

    switch (typeOfcars){
    case 1:
        cout << "\nType 1 is a car of 1500cc.\n";
        break;
    case 2:
        cout << "\nType 2 is a car of 1500cc with automatic mirrors.\n";
        break;
    case 3:
        cout << "\nType 3 is a car of 1500cc with automatic mirrors and front and rear sensor.\n";
        break;
    case 4:
        cout << "\nType 4 is a car of 1200cc.\n";
        break;
    case 5:
        cout << "\nType 5 is a car of 1200cc with automatic gear.\n";
        break;
    default:
        cout << "\nError! Please enter a valid number for the type of cars.\n";
    }
    return 0;
}
