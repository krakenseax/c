#include <iostream>

using namespace std;

int main()
{
    int hours, years, salary ;

    cout << "Please enter years of service: ";
    cin >> years;

    if (years > 30){
        cout << "Error! Please input valid data for years of service.\n";
        return 0;
    }
        cout << "Please enter hours worked: ";
        cin >> hours;

    if (hours > 60){
        cout << "Error! Please input valid data for hours worked.\n";
    }

    if ((years >= 15) && (years <= 30) && (hours <= 60)){
        if (hours > 40){
            salary = ((40 * 200) + ((hours - 40) * 300));
        }
        else{
            salary = (hours*200);
        }
        cout << "Total salary earned is RS" << salary << endl;
    }

    if ((years < 15) && (years <= 30) && (hours <= 60)){
        if (hours > 45){
            salary = ((45 *150) + ((hours - 45) * 250));
        }
        else{
            salary = (hours * 150);
        }
        cout << "Total salary earned for the week is RS" << salary << endl;
    }

    return 0;
}
