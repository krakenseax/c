#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int dependants, allowance, income, remainingAmount;
    float taxPayable;

    cout << "Please enter your annual income: RS ";
    cin >> income;

        if (income < 255000){
        cout << "Personal allowance(rs255000) will exceed annual income when deducting.\n";
        return 0;
        }

    cout << "\nPlease enter your number of dependants from 0 TO 3: ";
    cin >> dependants;

    switch (dependants){
    case 0:
        allowance = 255000;
        break;
    case 1:
        allowance = 325000;
        break;
    case 2:
        allowance = 395000;
        break;
    case 3:
        allowance = 425000;
        break;
    default:
        cout << "Error! Please input a valid number of dependants .\n";
        return 0;
    }

    remainingAmount = (income - allowance);

    if (remainingAmount <= 50000 ){
        taxPayable = ((0.15) * remainingAmount);
    }

    if ((remainingAmount > 50000) && (remainingAmount <= 120000)){
        taxPayable = ((0.15 * 50000) + ((0.2) * (remainingAmount - 50000)));
    }

    if (remainingAmount > 120000){
        taxPayable = (((0.15) * 50000) + ((0.2)  * 70000) + ((0.25) * (remainingAmount - 120000)));
    }

    cout << "Your tax payable is RS " << taxPayable << endl;

    return 0;
}
