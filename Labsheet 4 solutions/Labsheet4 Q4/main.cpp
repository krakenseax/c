#include <iostream>

using namespace std;

int main()
{
    int tariffNumber, unitConsumed;
    float minimumcharge, charge;

    cout << "Please enter your chosen Tariff number  between 110, 120 and 140 : ";
    cin >> tariffNumber;

    if ((tariffNumber != 110) && (tariffNumber != 120) && (tariffNumber != 140)){
        cout << "Error! Please enter a valid Tariff number.\n";
        return 0;
    }

    cout << "Please enter number of units consumed: ";
    cin >> unitConsumed;

    switch (tariffNumber){
    case 110:

        minimumcharge = 44;
        if (unitConsumed <= 25){
           charge = (unitConsumed * 2.75) ;
        }
        if ((unitConsumed > 25) && (unitConsumed < 75)){
            charge = ((25 * 2.75) + ((unitConsumed - 25) * 3.25));
        }
        if ((unitConsumed > 25 ) && (unitConsumed < 150)){
        charge = ((25 * 2.75) + (50 * 3.25) + ((unitConsumed - 75) * 4));
        }
        if (unitConsumed > 150){
            charge = ((25 * 2.75) + (50 * 3.25) + (75 * 4) + ((unitConsumed - 150) * 6.50));
        }
        if (unitConsumed == 0){
            charge = minimumcharge;
        }

    case 120:

        minimumcharge = 184;

        if (unitConsumed <= 25){
           charge = (unitConsumed * 3) ;
        }
        if ((unitConsumed > 25) && (unitConsumed < 75)){
            charge = ((25 * 3) + ((unitConsumed - 25) * 3.50));
        }
        if ((unitConsumed > 25 ) && (unitConsumed < 150)){
        charge = ((25 * 3) + (50 * 3.50) + ((unitConsumed - 75) * 4.25));
        }
        if (unitConsumed > 150){
            charge = ((25 * 3) + (50 * 3.50) + (75 * 4.25) + ((unitConsumed - 150) * 6));
        }
        if (unitConsumed == 0){
            charge = minimumcharge;
        }

    case 140:

        minimumcharge = 360;

        if (unitConsumed <= 25){
           charge = (unitConsumed * 3.25) ;
        }
        if ((unitConsumed > 25) && (unitConsumed < 75)){
            charge = ((25 * 3.25) + ((unitConsumed - 25) * 3.75));
        }
        if ((unitConsumed > 25 ) && (unitConsumed < 150)){
        charge = ((25 * 3.25) + (50 * 3.75) + ((unitConsumed - 75) * 4.50));
        }
        if (unitConsumed > 150){
            charge = ((25 * 3.25) + (50 * 3.75) + (75 * 4.50) + ((unitConsumed - 150) * 5.75));
        }
        if (unitConsumed == 0){
            charge = minimumcharge;
        }
    }

    cout << "The total amount you have to pay for the month is RS " << charge << endl;

    return 0;
}
