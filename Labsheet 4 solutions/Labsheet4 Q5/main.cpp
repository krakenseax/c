#include <iostream>

using namespace std;

int main()
{
    int salary, yearsOfService, carEngine, carAllowance;

     cout << "Please enter the employee salary RS: ";
     cin >> salary;

     if ((salary < 3000) || (salary > 100000)){
        cout << "Error! Please check whether you have enter valid data for salary.\n";
        return 0;
     }
     cout << "Please enter the years of service of the employee: ";
     cin >> yearsOfService;

     if ((salary >= 75000) && (salary <= 100000)){
        carEngine = 2000;
        carAllowance = 10000;
     }
     else if ((salary >= 60000) && (salary <75000)){
        carEngine = 1800;
        carAllowance = 8000;
     }
     else if ((salary >= 50000) && (salary < 60000) && (yearsOfService >= 10)){
        carEngine = 1800;
        carAllowance = 8000;
     }
     else if ((salary >= 50000) && (salary < 60000)){
        carEngine = 1600;
        carAllowance = 6000;
     }
     else if ((salary >= 40000) && (salary < 50000) && (yearsOfService >=20)){
        carEngine = 1500;
        carAllowance = 5000;
     }
     else if ((salary >= 30000) && (salary < 40000) && (yearsOfService > 25)){
        carEngine = 1400;
        carAllowance = 4000;
     }
     else {
        cout << "Sorry, You are not eligible for a free car Nor for a car allowance.\nToo bad for you!\n";
        return 0;
     }

     cout << "You are eligible for a free car of engine capacity " << carEngine << "cc OR you can take the Optional car allowance of RS " << carAllowance << endl;

    return 0;
}
