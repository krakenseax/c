#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int x;

    do{

        cout << "Enter an Integer" << endl;
        cin >> x;
        cout << "Square root of " << x << " is "<< sqrt(x) << endl;

    }while (x >= 0);

    cout << "END" << endl;


    return 0;
}
