#include <iostream>

using namespace std;

int main()
{
    int count, n, sum, choice, odd, div;

 do
 {
     cout << "Please enter the value of n : ";
     cin >> n ;
 }while (n <= 0);


    sum = 0;
    odd = 0;
    div = 0;
do
{


    cout << "\n**********************************" << endl;
    cout << "********Counting in Maths*********" << endl;
    cout << "**********************************" << endl;
    cout << "1.Sum of first n counting numbers" << endl;
    cout << "2.Sum of first nth odd numbers" << endl;
    cout << "3.Number of division by two" << endl;
    cout << "0.Exit" << endl;
    cout << "**********************************" << endl;
    cout << "Enter your choice (0-3):" << endl;
    cin >> choice;

   switch (choice)
   {

   case 1:
    for (count = 1 ; count <= n ; count ++)
    {
      sum += count;
    }
    cout << "\nyour sum is : " << sum << endl;
    sum = 0;
    break;

   case 2 :
    for (count = 1 ; count <= n ; count +=2)
    {
        odd += count;
    }
    cout << "\nsum of odd numbers " << odd << endl;
    odd = 0;
    break;

   case 3 :
    for (count = 1 ; count <= n ; count ++)
    {
        if ((count % 2) == 0)
        {
            div ++;
        }
    }
    cout << "\nThe number of times a whole number is divided by 2 :" << div << endl;
    div =0;
    break;
   }

}
while (choice != 0);
    return 0;
}
