#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int rabbit = 0, year = 0;

    cout << "Enter the number of Rabbits" << endl;
    cin >> rabbit;

    if (rabbit > 0){
        while (rabbit <= 1000){

            rabbit = (rabbit + ((0.2) * rabbit));
            trunc(rabbit);
            year++;

        }
        while((rabbit <= 1500) && (rabbit > 1000)){

            rabbit = (rabbit + ((0.1) * rabbit));
            trunc(rabbit);
            year++;

        }

    }else{

        cout << "Invalid Data" << endl;
    }

    cout << "Rabbit(s) = " << rabbit << endl;
    cout << "It will take " << year << " year(s) for the number of rabbits to exceed 1500" << endl;


    return 0;
}
