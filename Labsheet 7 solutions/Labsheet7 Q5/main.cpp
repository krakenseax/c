#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
    string data1, data2;
    ifstream read1, read2;
    ofstream write;

    read1.open("file1.txt");
    read2.open("file2.txt");
    write.open("target.txt");

        if (read1.fail() || read2.fail())
    {  cout << "Merging of file1.txt and file2.txt into target.txt COMPLETE! \n";

        cout << "Error opening file."<<endl;
    }

    while (getline(read1,data1))
           {
               write << data1 << endl;
           }
    while (getline(read2,data2))
           {
              write << data2 << endl;
           }

    cout << "Merging of file1.txt and file2.txt into target.txt COMPLETE! \n";

    read1.close();
    read2.close();
    write.close();

    return 0;
}
