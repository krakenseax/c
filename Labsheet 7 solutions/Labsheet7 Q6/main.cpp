#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    string id;
    ofstream write;
    string filename;

    cout << "Please enter the name of the file to store the student marks: ";
    getline(cin ,filename);

    write.open(filename.c_str());
    if (write.fail())
    {
        cout << "Error opening file" << endl;
    }

    float test1, test2, test3;
    write << "Student ID\tTest1\tTest2\tTest3\n";

    do
    {
        cout << "Please enter your student ID\n";
        getline (cin,id);
        if (id == "")
        {
            cout << "complete!" << endl;
            break;
        }
        cout << "Enter your mark for test1 , test2 and test3\n";
        cin >> test1 >> test2 >> test3;
        cin.ignore();

        write << id << "\t\t" << test1 << "\t" << test2 << "\t" << test3 << endl;

    }while (id != "");

    write.close();

    return 0;
}
