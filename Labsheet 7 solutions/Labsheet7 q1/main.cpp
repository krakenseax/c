#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    int num, square;
    ofstream write;

    write.open ("Squares.txt");
    cout << "Please input your numbers and press 0 when you are done.\n";
    cin >> num;

    write << "number\tsquare\n";
    while (num != 0)
    {
      square = (num * num);
      write << num << "\t" << square ;
      cout << " Enter another number\n";
      cin >> num;
      write << endl;
    }
    write.close();
    return 0;
}
