#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ofstream write;
    int region, num_of_region;
    float rainfall, evaporation;
    string filename;

    cout << "Please enter the name of the file to write : ";
    getline(cin ,filename);
    write.open(filename.c_str());


    if (write.fail())
    {
        cout << "Error opening file";
        return 0;
    }

    cout << "\nPlease enter the number of regions: ";
    cin >> num_of_region;

        write << "Region\trainfall\tevaporation\n";

    for (region = 1; region <= num_of_region ; region ++)
    {
        cout << "\nPlease enter the amount of rainfall in mm for region " << region <<endl;
        cin >> rainfall;
        cout << "Please enter the amount of evaporation in mm for region " << region << endl;
        cin >> evaporation;


        write << region << "\t" << rainfall << "\t\t" << evaporation << endl;

    }
    cout << "\nData entered!";

    write.close();

    return 0;
}
