#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ifstream read;
    int region, Hsurplus = 0 , Hdeficit = 0, Hdeficit_evaporation = 0, Hsurplus_evaporation = 0, Hdeficit_rainfall = 0, Hsurplus_rainfall = 0, null = 0, same = 0, count = 0;
    float rainfall, evaporation, water_deficit = 0, water_surplus = 0, highest_surplus = 0, highest_deficit = 0;
    string filename;

    cout << "Please enter the name of the file to write : ";
    getline(cin ,filename);
    read.open(filename.c_str());


    if (read.fail())
    {
        cout << "Error opening file";
        return 0;
    }

    read.ignore(27,'ch');

    while (read >> region >> rainfall >> evaporation)
    {
     water_deficit = 0;
     water_surplus = 0;

        if (rainfall > evaporation)
        {
            water_surplus = (rainfall - evaporation);

                if (water_surplus > highest_surplus)
                {
                    highest_surplus = water_surplus;
                    Hsurplus = region;
                    Hsurplus_rainfall = rainfall;
                    Hsurplus_evaporation = evaporation;
                }
        }

         else if (evaporation > rainfall)
        {
            water_deficit = (evaporation - rainfall);

                if (water_deficit > highest_deficit)
                {
                    highest_deficit = water_deficit;
                    Hdeficit = region;
                    Hdeficit_rainfall = rainfall;
                    Hdeficit_evaporation = evaporation;
                }
        }

        else if (rainfall = evaporation)
        {
            water_deficit = 0;
            water_surplus = 0;
            null = region;
            same = rainfall;
            cout << "Region "<< null << " has same rainfall and evaporation of " << same <<  " mm.\n";
        }

    }
    cout << "Region "<< Hsurplus << " has the highest water surplus of "<< highest_surplus << " mm with rainfall " << Hsurplus_rainfall << " mm and evaporation of " << Hsurplus_evaporation << " mm.\n";
    cout << "Region "<< Hdeficit << " has the highest water deficit of " << highest_deficit << " mm with rainfall " << Hdeficit_rainfall << " mm and evaporation of " << Hdeficit_evaporation << " mm.\n";

    read.close();
    return 0;
}
