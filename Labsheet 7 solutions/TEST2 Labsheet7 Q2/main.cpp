#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream read;
    string number, square;

    read.open ("squares.txt");

    while (read >> number >> square)
        {
         cout << number << "\t" << square << endl;
        }

    read.close();

    return 0;
}
