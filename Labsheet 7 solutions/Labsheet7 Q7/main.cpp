#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;

int main()
{
    float test1, test2, test3, average = 0, highest_average = -9999;
    string filename, id, AvgID;
    ifstream read;

    cout << "Please enter the name of the file to open : ";
    getline(cin ,filename);
    read.open(filename.c_str());

    if (read.fail())
    {
        cout << "Error opening file";
        return 0;
    }

    read.ignore(28,'ch');

    cout << "Student ID\tTest1\tTest2\tTest3\tAverage mark\n";

    while (read >> id >> test1 >> test2 >> test3)
    {
        average = ((test1 + test2 + test3) / 3 );
        cout << id << "\t\t" << test1 << "\t" << test2 << "\t" << test3 << "\t" << average << endl;
        if (average > highest_average )
        {
           highest_average = average;
           AvgID = id;
        }

    }
    cout << "The highest average marks is " << highest_average << " and is obtained by student id : " << AvgID << endl;
    read.close();

    return 0;
}
