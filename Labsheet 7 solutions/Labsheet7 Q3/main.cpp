#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream read;
    string number, square;
    int data,largest= -99999 , smallest= 999999, count=0 ;
    double sum = 0;

    read.open ("squares.txt");
    read.ignore(13,'ch');
    while (read >> data)
        {
            if (data > largest)
            {
                largest = data;
            }
            if (data < smallest)
            {
                smallest = data;
            }
         //cout << data << endl;
         count++;
         sum += data;
        }
    read.close();
    cout << "sum of all numbers is " << sum << endl;
    cout << "Average is " << (sum/count) << endl;
    cout << "largest number is " << largest << endl;
    cout << "smallest number is " << smallest << endl;

    ofstream write;
    write.open("Analysis.txt");
    write << "sum     \t" << sum << endl;
    write << "Average \t" << (sum/count) << endl;
    write << "Largest \t" << largest << endl;
    write << "Smallest \t" << smallest << endl;
    write.close();
    return 0;
}
