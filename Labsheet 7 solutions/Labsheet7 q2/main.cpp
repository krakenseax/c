#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream read;
    string data;

    read.open ("squares.txt");
    if (read.fail())
    {
        cout << "Error opening file." <<endl;
    }

    while (getline(read,data))
        {
         cout << data << endl;
        }

    read.close();

    return 0;
}
