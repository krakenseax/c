#include <iostream>
#include <cmath>
using namespace std;

int poly(int x)
{
   double ans;
   ans = pow(x,4) + pow (x,3) + (3 * pow (x,2)) + 2;
   return ans;
}
int main()
{
    int num, p;
    cout << "Enter value of N" << endl;
    cin >> num;

    p = poly(num);
    cout << "The poly is " << p << endl;
    return 0;
}
