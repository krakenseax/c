#include <iostream>
#include <cmath>

using namespace std;

double cubecalc (double x)
{
    return (pow(x,3));
}
int main()
{
    int side, cost, volume, totalcost;
    cout << "Please enter the length of one side of a cube" << endl;
    cin >> side;
    cout << "Please enter the cost per cubic metre of petrol" << endl;
    cin >> cost;

    volume = cubecalc(side);
    totalcost = (volume * cost);

    cout << "The volume of the cube is " << volume << " and your total cost for the petrol is RS " << totalcost << endl;
    return 0;
}
