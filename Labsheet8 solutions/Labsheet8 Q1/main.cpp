#include <iostream>

using namespace std;

int IntSum(int x)
{
   int count = 0;
   int sum = 0;

   while (count <= x)
   {
       sum += count;
       count ++;
   }
   return sum;
}
int main()
{
    int num, totalsum;
    cout << "Input a positive number to calculate sum. \n";
    cin  >> num;

    totalsum = IntSum(num);
    cout << "Total sum of " << num << " is " << totalsum << endl;

    return 0;
}
