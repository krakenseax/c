#include <iostream>

using namespace std;

int small (int a, int b)
{
    int smaller ;
    if (a < b )
    {
        smaller = a;
    }
    else
    {
        smaller = b;
    }

    return smaller;
}

int main()
{
    int num1 , num2, ans ;
    cout << "Input 2 numbers to find out which is smaller: " << endl;
    cin >> num1 >> num2;

    ans = small(num1,num2);

    cout << ans << " is the smaller between " << num1 << " and " << num2;
    return 0;
}
