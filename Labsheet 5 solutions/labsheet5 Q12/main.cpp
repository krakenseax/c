#include <iostream>
#include <cmath>

using namespace std;
// calculate compound interest
int main()
{
    int years, i;
    double interest_rate, investment, double_amount;

    cout << "Please enter the amount you will invest: MUR ";
    cin >> investment;
    cout << "Enter interest rate in %: ";
    cin >> interest_rate;

    i = 1;
    interest_rate = (interest_rate / 100);
    years = 0;
    double_amount = (2 * investment);

    while (investment < double_amount)

    {
        //interest = (p * r);
        investment += (investment * interest_rate);
        years ++;
    }
    cout << "It will take "<< years << " years for your investment of MUR " << abs(double_amount / 2) << " to double." << endl;

    return 0;
}
