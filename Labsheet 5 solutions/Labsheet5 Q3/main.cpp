#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n, sum, multiples, x ;

    cout << "Please enter a value:  ";
    cin >> n;

    multiples = 1;
    x = 1;
    sum = 0;
    while (multiples < (n - 1))
    {
        multiples = (x * 3);
        sum = (sum + (multiples * multiples));
        x++;
        cout << "multiples of 3 are " << multiples <<endl;
    }
    cout << "Sum of square is " << sum << endl;

    return 0;
}

