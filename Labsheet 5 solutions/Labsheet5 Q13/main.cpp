#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int num_of_inputs, inputs, sum, i;


    cout << "How many numbers will you enter?  :";
    cin >> num_of_inputs;
    cout <<"Now enter some numbers and i will output the even numbers and their sum \n";


    i = 1;
    sum = 0;


    while (i <= num_of_inputs )
    {
        cin >> inputs;
        if (inputs % 2 ==0)
        {
         cout << inputs << " is an even number.\n";
         sum += inputs;
        }
        else
        {
            cout << inputs << " is an odd number.\n";
        }
        i ++;
    }

   cout << "\nSum of all these even numbers is :  " << sum << endl;

    return 0;
}
