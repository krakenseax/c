#include <iostream>

using namespace std;

int main()
{
    int set_of_num, i, num_left, smallest, largest;
    float individual_Num, average, sum;

    cout << "Please enter the set of numbers to be input :";
    cin >> set_of_num;

    i = 1;
    sum = 0;
    smallest = 99999;
    largest= -99999;

    cout << "please enter the individuals numbers: \n";
    while (i <= set_of_num)
    {

        cin >> individual_Num;

        if (individual_Num < smallest)
        {
            smallest = individual_Num;
        }
        if (individual_Num > largest){
                largest = individual_Num;
            }

        num_left = (set_of_num - i);
        cout << "To be able to calculate average of a set of "<< set_of_num << " You still have to enter: " << num_left << " number.\n";
        sum += individual_Num;
        average = (sum / set_of_num);
        i++;
    }
    cout << "\nYour Average is " << average << endl;
    cout << "\nYour largest number is " << largest << endl;
    cout << "\nYour smallest number is " << smallest << endl;
    return 0;
}
