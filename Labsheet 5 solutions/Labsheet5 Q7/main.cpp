#include <iostream>

using namespace std;

int main()
{
    int n, i, sum;
    cout << "please enter an integer: ";
    cin >> n;

    i = 1;
    sum = 0;

    while (i <= n){
        sum = sum + i;
        i++;
    }
    cout << "Your sum is " << sum << endl;
    return 0;
}
