#include <iostream>

using namespace std;
// calculate compound interest
int main()
{
    int n, i;
    double r, p;

    cout << "Please enter your amount of money: ";
    cin >> p;
    cout << "Enter interest rate in %: ";
    cin >> r;
    cout << "Enter number of years for which the money has remained in the bank: ";
    cin >> n;

    i = 1;
    r = (r / 100);

    while (i <= n)
    {
        //interest = (p * r);
        p += (p * r);
        cout << "\nAt the end of  year " << i << " you will have RS " << p << endl;
        i++;
    }

    return 0;
}
