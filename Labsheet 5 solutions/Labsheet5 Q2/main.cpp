#include <iostream>

using namespace std;

int main()
{
    int n, sum,multiples;

    n = 1;
    multiples = 1;
    sum = 0;

    while (n < 20)
        {
        multiples = (n * 5);
        n++;
        sum += multiples;
        }
        cout << "Sum of Multiples of 5 are :" << sum << endl;
    return 0;
}
