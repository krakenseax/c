#include <iostream>

using namespace std;

int main()
{
    int n, multiples;
    n = 1;
    multiples = 1;

    while (n <= 20) {
        multiples = (n * 5);
        n++;
        cout << "Multiples of 5 are :" << multiples << endl;
    }
    return 0;
}
