#include <iostream>

using namespace std;

int main()
{
    int set_of_num, i;
    float average, individual_Num, sum;

    cout << "Please enter the set of numbers to be input :";
    cin >> set_of_num;

    i = 1;
    sum = 0;

    cout << "please enter the individuals numbers: \n";
    while (i <= set_of_num)
    {

        cin >> individual_Num;
        sum += individual_Num;
        average = (sum / set_of_num);
        i++;
    }
    cout << "Your average is " << average << endl;
    return 0;
}
