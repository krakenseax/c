#include <iostream>
using namespace std;

int main()
{
    unsigned int n;
    unsigned int factorial = 1;
    int i;

    cout << "Enter a positive integer: ";
    cin >> n;

    i = 1;

    while (i <= n)
    {
        factorial *= i;
        i++;
    }

    cout << "Factorial of " << n << " = " << factorial;
    return 0;
}
