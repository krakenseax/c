#include <iostream>

using namespace std;

int main()
{
    int n;
    double i, sum, reciprocal;
    cout << "please enter an integer: ";
    cin >> n;

    i = 1;
    sum = 0;

    while (i <= n){
        reciprocal = (1 / i);
        sum = sum + reciprocal;
        i++;
    }
    cout << "Your sum is " << sum << endl;
    return 0;
}
