#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    float a,b,c ;
    cout <<"This program find real roots to a quadratic "<< endl;
    cout << "Please enter the coefficient a b c : \n";
    cin >>a >> b >> c;

    float discRoot = sqrt(b*b-4*a*c);
    float root1 = (-b+discRoot)/(2*a);
    float root2 = (-b-discRoot)/(2*a);

    cout << "The solution are : "<< root1 << " and " << root2 << endl;

    return 0;
}
