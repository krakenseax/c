#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double x1,x2,y1,y2,distance ;

    cout << "Enter the points for the first coordinates (x,y) \n";
    cin >> x1 >>y1 ;
    cout <<"Enter the points for your secnd coordinates (x,y) \n";
    cin >>x2 >>y2 ;

    distance = sqrt((pow(x1-x2,2)-pow(y1-y2,2)));

    cout << "Distance between two points is: " << distance ;
    return 0;
}
